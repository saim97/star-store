<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* header.html.twig */
class __TwigTemplate_32dbe0810be71ab29631fbe6b8bb5537bc4551c3c2420f534f6ba35ad00f6621 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "header.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "header.html.twig"));

        // line 1
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 4
        echo "    <head>
        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">
        <!--Get your code at fontawesome.com-->
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\" > </script>

        <script src=\"https://code.jquery.com/jquery-latest.min.js\"></script>

        <link rel=\"stylesheet\" href=\"assets/css/intlTelInput.css\">
        <script src=\"assets/js/intlTelInput.min.js\"> </script>
";
        // line 16
        echo "    </head>

<header>
<title>StarStore</title>
    <body>
        <div class=\"contact\">
            <div id=\"email\">
                <i class=\"fa fa-envelope\"></i>
                <a href=\"www.google.com\">support@starstore.com </a>
            </div>
        </div>
        <h1 id=\"test\">StarStore</h1>
        <input type=\"text\" id=\"try\" value=\"Lets Try\">
        <p id=\"show\"></p>
    </body>
</header>

<script>
//    \$('#try').intlTelInput(\"getNumber\");

    \$('#try').intlTelInput({
        initialCountry: \"auto\",
        allowDropdown: true,
    });

</script>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 1
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 2
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("page_header");
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "header.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  102 => 2,  92 => 1,  57 => 16,  46 => 4,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% block stylesheets %}
{{ encore_entry_link_tags('page_header')  }}
{% endblock %}
    <head>
        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">
        <!--Get your code at fontawesome.com-->
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\" > </script>

        <script src=\"https://code.jquery.com/jquery-latest.min.js\"></script>

        <link rel=\"stylesheet\" href=\"assets/css/intlTelInput.css\">
        <script src=\"assets/js/intlTelInput.min.js\"> </script>
{#        <script src=\"{{ asset('assets/js/intlTelInput.min.js') }}\"></script>#}
{#        <script src=\"{{ asset('assets/js/intlTelInput.min.js') }}\"></script>#}
{#        <script src=\"{{ asset('assets/js/utils.js') }}\"></script>#}
    </head>

<header>
<title>StarStore</title>
    <body>
        <div class=\"contact\">
            <div id=\"email\">
                <i class=\"fa fa-envelope\"></i>
                <a href=\"www.google.com\">support@starstore.com </a>
            </div>
        </div>
        <h1 id=\"test\">StarStore</h1>
        <input type=\"text\" id=\"try\" value=\"Lets Try\">
        <p id=\"show\"></p>
    </body>
</header>

<script>
//    \$('#try').intlTelInput(\"getNumber\");

    \$('#try').intlTelInput({
        initialCountry: \"auto\",
        allowDropdown: true,
    });

</script>", "header.html.twig", "/var/www/html/StarStore/templates/header.html.twig");
    }
}
