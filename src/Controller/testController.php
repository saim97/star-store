<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\Common\Annotations\Annotation;
use Symfony\Component\HttpFoundation\Response;

class testController extends AbstractController
{
    public function tester()
    {
        return new Response(" Welcome to first Page of Star Store ");
    }
}