<?php

//echo 'Beta';

/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;

// If you don't want to setup permissions the proper way, just uncomment the following PHP line
// read http://symfony.com/doc/current/book/installation.html#configuration-and-setup for more information
//umask(0000);

// This check prevents access to debug front controllers that are deployed by accident to production servers.
// Feel free to remove this, extend it, or make something more sophisticated.

$x_forwarded_ip = explode(',', @$_SERVER['HTTP_X_FORWARDED_FOR']);
$ip_array = array('119.73.120.181', '124.29.220.137', '176.9.63.138', '127.0.0.1', '203.215.175.27', '203.215.181.10', '54.85.21.160', '137.59.230.95', '124.29.220.138', 'fe80::1', '::1');
if (//isset($_SERVER['HTTP_CLIENT_IP'])
    //|| isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    !(in_array(@$x_forwarded_ip[0], $ip_array) || php_sapi_name() === 'cli-server')
    and !(in_array(@$x_forwarded_ip[1], $ip_array) || php_sapi_name() === 'cli-server')
    and !(in_array(@$_SERVER['REMOTE_ADDR'], $ip_array) || php_sapi_name() === 'cli-server')
) {
    header('HTTP/1.0 403 Forbidden');
//    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
    exit('You are not allowed to access this page.');
}

require __DIR__ . '/../vendor/autoload.php';
Debug::enable();
//$loader = require_once __DIR__ . '/../app/bootstrap.php.cache';

require_once __DIR__ . '/../app/AppKernel.php';

$kernel = new AppKernel('dev', true);

$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
